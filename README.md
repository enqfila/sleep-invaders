# Sleep Invaders 

This is a fork of a project originally made for USP Game Dev's training "jam" in 2023. The objective of this fork is to port the game to different engines, as a means to learn about them without worrying about other aspects such as game design, art, music, etcetera.

## About the original authors

This project would not be possible without the hard work of:

- Our tutor @Guilhermev25 that helped us learn a lot about Godot 3.5, pixel art, Git, and solving A LOT of problems that surfaced during development
- Our programmers @enzospinella @AndreLdev and @Alois.io that made most of the implementation
- Our artist @oliverprioli that gave life and color to our game, and also gave guidelines to the audio work
- Myself, mostly working on the audio, but also ironing out some code issues and linking the art with mechanics

# Next Steps

- Correcting the intro animation for better sync between audio and video in web in the legacy project (to-do on the original as well)
- Porting to Unreal Engine 5
- Porting to Godot 4
- Porting to Unity 2023
