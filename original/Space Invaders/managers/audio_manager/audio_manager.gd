extends Node

export var master_volume : int = 100
export var music_volume : int = 100
export var sfx_volume : int = 100

export var intro_music : AudioStreamSample = null
export var loop_music : AudioStreamSample = null
export var end_music : AudioStreamSample = null
var fanfare : AudioStreamSample = null
var death : AudioStreamSample = null

onready var intro : AudioStreamPlayer = $"Intro Player"
onready var loop : AudioStreamPlayer  = $"Loop Player"
onready var end : AudioStreamPlayer = $"End Player"

# Talvez fazer isso ser controlável? 
onready var sfx_channel : Array = [$"SFX Channel 1", $"SFX Channel 2", $"SFX Channel 3"]
var sfx_played : Array = [null, null, null]
# Talvez guardar os efeitos aqui e quando o SFX terminar ele limpar, e se entrar o mesmo efeito só dar play do 0
var sfx_free : Array = [0,0,0]

# API

# Called by many entities that operate under SFX
func play_SFX(sfx : AudioStreamSample):
	var channel_value = 3
	var target_channel = check_SFX_ready(sfx)
	
	if (target_channel < 0):
		for index in len(sfx_free):
			if channel_value > sfx_free[index]:
				target_channel = index
	occupy_SFX_channel(target_channel, sfx)
	# Checar qual canal está mais livre (menor valor)
	# ocupar o canal[index] mais livre
	# chamar occupy_SFX_channel(index)
	# trocar o stream do SFX_channel correspondente
	# dar play no SFX_channel correspondente
	pass

func load_music():
	loop.autoplay = false
	intro.stream = intro_music
	loop.stream = loop_music
	end.stream =  end_music
	_on_level_start() #Debug
# Inner Workings

func _ready():
	_on_master_volume_change()
	load_music()
	

func _process(_delta):
	_on_master_volume_change()
	
func volume_to_db_converter(volume):
	return (3.0/5.0)*volume - 80;
	
func _on_master_volume_change():
	intro.volume_db = volume_to_db_converter(master_volume/100.0 * music_volume);
	loop.volume_db = volume_to_db_converter(master_volume/100.0 * music_volume);
	end.volume_db = volume_to_db_converter(master_volume/100.0 * music_volume);
	sfx_channel[0].volume_db = volume_to_db_converter(master_volume/100.0 * sfx_volume);
	sfx_channel[1].volume_db = volume_to_db_converter(master_volume/100.0 * sfx_volume);
	sfx_channel[2].volume_db = volume_to_db_converter(master_volume/100.0 * sfx_volume);
	
func _on_music_volume_change():
	pass
func _on_sfx_volume_change():
	pass	


func _on_dead():
	music_volume = 0
	_on_master_volume_change()
	#play_SFX(death)
	
func _on_level_start():
	
	intro.play(0.0)
	
func _on_Intro_Player_finished():
	loop.autoplay = true
	loop.play(0.0)
	
func _on_level_finished():
	end.play()
	
func _on_End_Player_finished():
	pass

func check_SFX_ready(sfx: AudioStreamSample):
	for index in len(sfx_played):
		if sfx_played[index] == sfx:
			return index
	return -1

func occupy_SFX_channel(index : int, sfx : AudioStreamSample):
	# Quando um SFX entra, aquele canal fica "fresco" (valor alto) e os outros "envelhecem"
	sfx_free[index] = 4
	for channel in len(sfx_free):
		if (sfx_free[channel] > 0):
			sfx_free[channel] -= 1
		else:
			sfx_played[channel] = null 
	sfx_channel[index].stream =sfx 
	sfx_played[index] = sfx
	sfx_channel[index].play()
	


