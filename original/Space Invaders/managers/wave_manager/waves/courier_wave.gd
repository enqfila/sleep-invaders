extends WaveManager
class_name CourierWave
export var forced_item : PackedScene = null;


func spawn(item):
	var item_to_spawn = item
	if (forced_item != null):
		item_to_spawn = forced_item
	var new_follower = path_follow.instance();
	new_path.add_child(new_follower);
	new_follower.add_child(follower_type.instance());
	new_follower.get_child(0).set_drop_item(item_to_spawn);
	new_follower.set_velocity(followers_velocity);
