extends Node

export var weapon : PackedScene = null;

func apply(target):
	if (target.has_method('change_weapon')):
		target.change_weapon(weapon)
