class_name Item 
extends Node2D

export var velocity: int = 1
onready var effect: Node = $Effect

func _physics_process(delta):
	position.y += velocity * delta

"""
 Essa função é padrão, e não precisa necessariamente ter um alvo
 Como, por exemplo, se quisermos implementar um 
 drop salvador de fim de jogo que causa dano em todos os gatineos
"""
func _on_Item_body_entered(body):
	if body is Player:
		effect.apply(body)
		queue_free()
