extends Enemy

export var delay_between_spray : float = 7.5;
export var delay_during_spray : float = 1;
export var spray_size : int = 3;
var wpn = null
var bullets_shot = 0;

func _ready():
	wpn = .get_weapon()
	wpn.delay = delay_between_spray;
	
func _on_bullet_fired():
	._on_bullet_fired()
	if (bullets_shot == 0):
		wpn.delay = delay_during_spray
	bullets_shot += 1
	if (bullets_shot > spray_size -1):
		wpn.delay = delay_between_spray
		bullets_shot = 0
