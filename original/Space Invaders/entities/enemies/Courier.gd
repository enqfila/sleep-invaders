extends Enemy
class_name Courier

export var drop_item: PackedScene
onready var death_effect : Node = $"Death Effect"


func set_drop_item(item: PackedScene):
	self.drop_item = item;

func _on_Health_dead():
	death_effect.effect(drop_item);
	._on_Health_dead()

