class_name Health
extends Node

signal health_changed(new_health)
signal damage_taken()
signal heal_taken()
signal dead()
export(int) var max_health = 3
onready var health: int = max_health

func take_damage(damage: int):
	health = health - damage
	if health < 1:
		emit_signal("dead")
		emit_signal("health_changed", 0)
	else:
		emit_signal("damage_taken")
		emit_signal("health_changed", health)


func take_heal(heal: int):
	health = int(min(max_health, health + heal))
	emit_signal("heal_taken")
	emit_signal("health_changed", health)

func get_current_health():
	return health

func change_max_health(new_health: int):
	self.health = new_health;

