extends Area2D

signal player_entered(body)
signal enemy_entered(body)
signal bullet_entered(area)
signal bullet_enemy_entered(area)
signal item_entered(area)


func _on_HitBox_area_entered(area):
	if area is Bullet:
		emit_signal("bullet_entered", area)
	if area is Bullet_Enemy:
		emit_signal("bullet_enemy_entered", area)
	if area is Item:
		emit_signal("item_entered", area)


func _on_HitBox_body_entered(body):
	if body is Enemy:
		emit_signal("enemy_entered", body)
	if body is Player:
		emit_signal("player_entered", body)
