extends AnimatedSprite

var initial_speed = 0.5;
var final_speed = 1;
var step = final_speed - initial_speed;
var animation_length = 5;
func special_shoot_animation():
	step = 0;
	self.speed_scale = 1.5;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _on_frame_changed():

	if (self.frame > 0 and step < final_speed - initial_speed):
		self.speed_scale = initial_speed + step

		step += (final_speed - initial_speed)/animation_length;
		if (self.speed_scale >= final_speed):
			self.speed_scale = final_speed;


func _on_animation_finished():

	self.speed_scale = final_speed
	step = final_speed - initial_speed
