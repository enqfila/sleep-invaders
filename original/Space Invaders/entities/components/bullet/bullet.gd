class_name Bullet
extends Area2D

signal play_sfx(sfx);

export var direction: Vector2;
export var velocity: float;
export var damage: int;
export var instance_sfx : AudioStreamSample = null;
export var hit_sfx : AudioStreamSample = null;

onready var animation : AnimatedSprite = $AnimatedSprite
var collided: bool = false
onready var effect = get_node("Hit_effect")

func _ready():
	animation.play("shoot");
	self.connect("play_sfx",get_tree().root.get_child(1),'_on_sfx_played') # links to Level
	if (instance_sfx != null):
		emit_signal("play_sfx",instance_sfx)
	effect.readyeffect()
	
	
func _physics_process(delta):
	self.position += direction * velocity * delta;
	


func _on_Bullet_body_entered(body):
	if not collided and body is Enemy:
		collided = true
		if (hit_sfx != null):
			emit_signal("play_sfx",hit_sfx)
		if (animation.frames.get_animation_names().has('hit')):
			animation.play("hit")
		effect.super_effect()
		


