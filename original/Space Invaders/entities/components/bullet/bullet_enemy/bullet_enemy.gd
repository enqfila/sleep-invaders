class_name Bullet_Enemy
extends Area2D

export var direction: Vector2
export var velocity: float
export var damage: int
onready var sprite : AnimatedSprite = $AnimatedSprite
var collided: bool = false
onready var effect = get_node("EnemyHitEffect")


func _ready():
	sprite.play('shoot')

func _physics_process(delta):
	self.position +=  direction * velocity * delta;


func _on_Bullet_body_entered(body):
	if not collided and body is Player:
		collided = true
		hit()


func hit():
	self.velocity = 0
	sprite.play('hit')
	self.queue_free()


func limit_hit():
	if not collided:
		collided = true
		sprite.play('hit')
		effect.super_effect()

