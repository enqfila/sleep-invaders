extends Node2D
class_name LevelManager

onready var audio_manager : Node = $AudioManager;
onready var item_timer := $ItemTimer as Timer;
onready var courier_wave := $CourierWave as CourierWave;
onready var health_drop := preload("res://entities/item/items/health_item.tscn")

export var next_scene : PackedScene;
export var first_item_cooldown := 0.0;
export var item_percentage_cooldown := 0.0;
export(Array, PackedScene) var available_items = [];
onready var can_spawn_items : bool = len(available_items) > 0
var enemy_counter = 0;
var probability_item_spawn := 20;

func _ready():
	randomize();
	item_timer.start(first_item_cooldown);
	for i in range(self.get_child_count()):
		var node = get_child(i);
		if node is Moment:
			for child in node.get_children():
				if child is WaveManager:
					enemy_counter += child.followers_number;
				
func on_enemy_death():
	enemy_counter -= 1;
	if enemy_counter == 0:
		finish_level();

func _on_sfx_played(sfx):
	audio_manager.play_SFX(sfx);

func game_over():
	Autoload.current_level = get_tree().current_scene.filename
	$Transition.transition_to("res://menus/game_over/game_over.tscn");
	
func finish_level():
	$Transition.transition_to(next_scene.resource_path);
	
func will_item_spawn(): 
	var random_n = int(rand_range(0, 101));
	var will = true;
	
	if (random_n > probability_item_spawn):
		will = false
		if (self.probability_item_spawn < 100):
			self.probability_item_spawn += 5;
	else:
		if (self.probability_item_spawn > 0):
			self.probability_item_spawn -= 5;
		
	return will and can_spawn_items;

func which_item_spawn():
	if (len(available_items) > 0):
		var qual_item = int(rand_range(0, len(available_items)));
		return available_items[qual_item];

func _on_ItemTimer_timeout():
	item_timer.stop();
	if (will_item_spawn()):
		courier_wave.spawn(which_item_spawn());
	self.first_item_cooldown -= self.first_item_cooldown * (self.item_percentage_cooldown/100.0);
	item_timer.start(first_item_cooldown);


func _on_Limits_kill_player():
	audio_manager._on_dead()
	$Player.dead()


func _on_Player_health_changed(health_amount):
	if (health_amount == 1):
		courier_wave.spawn(health_drop)
