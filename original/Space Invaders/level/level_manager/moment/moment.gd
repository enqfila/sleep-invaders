extends Node
class_name Moment

onready var timer := $MomentTimer as Timer;
export var time_moment := 0;


func _ready():
	timer.start(time_moment);
	for wave_managers in self.get_children():
		wave_managers.set_process(false);

func _on_Timer_timeout():
	for wave_managers in self.get_children():
		wave_managers.set_process(true);
	
		if wave_managers.has_method('spawn'):
			wave_managers.spawn(wave_managers.forced_item)
	timer.stop()
