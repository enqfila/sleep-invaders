extends Node2D

signal kill_player()

func _on_EnemyArea_body_entered(body):
	if body is Enemy:
		emit_signal("kill_player");


func _on_EnemyArea_bullet_entered(bullet):
	if (bullet is Bullet_Enemy):
		if (bullet.has_method('limit_hit')):
			bullet.limit_hit()


func _on_BulletLimit_area_entered(area):
	if area is Bullet or area is Bullet_Enemy:
		area.queue_free()
