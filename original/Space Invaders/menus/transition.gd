extends CanvasLayer

export(String, FILE, "*.tscn") var next_scene_path


func transition_to(next_scene: String = next_scene_path):
	$AnimationPlayer.play_backwards("Fade")
	yield($AnimationPlayer, "animation_finished")
	get_tree().change_scene(next_scene)
