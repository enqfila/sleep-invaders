extends Node

var during_opening : bool = true
onready var animator : AnimationPlayer = $OpeningAnimator

func check_if_opening_skipped():
	if Input.is_action_pressed("shoot"):
		animator.stop()
		animator.play("title")
		animator.clear_queue()
		during_opening = false
	pass
	
func _process(_delta):
	if (during_opening):
		check_if_opening_skipped()

func _ready():
	animator.queue('title')


func _on_OpeningAnimator_animation_finished(anim_name):
	if (anim_name == 'start'):
		during_opening = false
